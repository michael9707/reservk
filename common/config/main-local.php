<?php

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=bingo',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to FALSE and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => FALSE,
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp-relay.gmail.com',
//                'username' => 'proyectos.contratos@autonoma.edu.co',
//                'password' => 'xurrsgghlkgymcix',
//                'port' => '587',
//                'encryption' => 'tls',
//            ], 
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'ambientes.rev@gmail.com',
                'password' => 'ambientes2999',
                'port' => '587',
                'encryption' => 'tls',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => TRUE,
                        'verify_peer' => FALSE,
                        'verify_peer_name' => FALSE,
                    ],
                ],
            ],
        ],
    ],
];
