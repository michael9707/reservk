<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model {

    public $username;
    public $password;
    public $rememberMe = true;
    private $_user;
    const ESTADO_INACTIVO = 0;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'username' => 'Usuario','style="margin-left:34px;"',
            'password' => 'Contraseña',
            'rememberMe' => 'Recordarme',
            'estado' => 'Estado',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params) {
        $message = "";
        if (!$this->hasErrors()) {
            $user = $this->getUser();                       
            if (!$user || !$user->validatePassword($this->password)) {
<<<<<<< HEAD
                $message ="Contraseña o usuario incorrecto";
=======
                $this->addError($attribute, 'Contraseña o usuario incorrecto');
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
            }
            if(!$user || $user->estado ==0){
                $message = "Esta cuenta esta INACTIVADA";
            }   
            if($message != null)
            $this->addError($attribute,$message);
        }
    }
    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login() {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser() {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }

}
