<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
<<<<<<< HEAD
=======
            ['name', 'required'],
            ['name', 'string', 'min' => 5, 'max' => 20],
            ['name', 'match', 'pattern' => '/[A-Za-z]+/','message' => 'Solamente letras'],
            ['surname', 'required'],
            ['surname', 'string', 'min' => 5, 'max' => 20],
<<<<<<< HEAD
            ['surname', 'match', 'pattern' => '/[A-Za-z]/','message' => 'Solamente letras'],
            ['id_number', 'required'],
            ['id_number', 'string', 'min' => 10, 'max' => 10],
            ['id_number', 'unique', 'targetClass' => '\common\models\User', 'message' => 'El número de identificación indicado ya esta siendo usado por otro usuario.'],
            ['id_number', 'match', 'pattern' => '/[0-9]/','message' => 'Solamente numeros'],
=======
            ['surname', 'match', 'pattern' => '/[A-Za-z]+/','message' => 'Solamente letras'],
            ['id_number', 'required'],
            ['id_number', 'string', 'min' => 10, 'max' => 10],
            ['id_number', 'unique', 'targetClass' => '\common\models\User', 'message' => 'El número de identificación indicado ya esta siendo usado por otro usuario.'],
            ['id_number', 'match', 'pattern' => '/[0-9]+/','message' => 'Solamente numeros'],
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
>>>>>>> 5aa45dce5abc69c8d67fa679d76bfd8dfeb0f115
            ['username', 'trim'],
            ['username', 'required'],

            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'El nombre de usuario indicado ya esta siendo usado.'],
            ['username', 'string', 'min' => 10, 'max' => 30],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'min' => 10,'max' => 30],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'El correo electronico ya esta siendo usado.'],

            ['password', 'required'],
<<<<<<< HEAD
            ['password', 'string', 'min' => 6],
=======
            ['password2', 'required'],
            ['password', 'match', 'pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{5,10}/','message' => 'Mínimo 8 y máximo 10 caracteres al menos 1 alfabeto en mayúsculas, 1 alfabeto en minúsculas, 1 número y 1 carácter especial'],
            ['password2', 'match', 'pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{5,10}/','message' => 'Mínimo 8 y máximo 10 caracteres al menos 1 alfabeto en mayúsculas, 1 alfabeto en minúsculas, 1 número y 1 carácter especial'],
            ['password', 'string', 'min' => 8,'max' => 12],
            ['password2', 'compare', 'compareAttribute'=>'password'],
            ['email', 'compare', 'compareAttribute'=>'username'],

>>>>>>> 5aa45dce5abc69c8d67fa679d76bfd8dfeb0f115
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'username' => 'Usuario',
            'email' => 'Correo electrónico',
            'password' => 'Contraseña',
            'rememberMe' => 'Recordarme',
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->estado = User::ESTADO_INACTIVO;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        if(!$user->save()){
            print_r($user->getErrors());
            die();
        }
        return $user->save() && $this->sendEmail($user);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'JAGAO'])
            ->setTo($this->email)
            ->setSubject('Verificación cuenta registrada en JAGAO.CO')
            ->send();
    }
}