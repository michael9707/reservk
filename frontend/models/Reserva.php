<?php

namespace frontend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "reserva".
 *
 * @property int $id
 * @property int $title
 * @property string $body
 * @property string $type_event
 * @property string $url
 * @property string $class
 * @property string $start
 * @property string $end
 * @property string $inicio_normal
 * @property string $final_normal
 * @property string $status
 * @property int $limit_events
 * @property int $usuario_id
 *
 * @property DanoAmbiente[] $danoAmbientes
 * @property Ambiente[] $titles
 * @property Usuario $usuarioIdUsuario
 * @property Ambiente $title0
 */
class Reserva extends \yii\db\ActiveRecord
{
    public $title2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'body', 'type_event', 'url', 'class', 'start', 'end', 'inicio_normal', 'final_normal', 'status', 'usuario_id'], 'required'],
            [['title', 'limit_events', 'usuario_id'], 'integer'],
            [['body'], 'string'],
            [['inicio_normal', 'final_normal'], 'safe'],
            [['type_event'], 'string', 'max' => 50],
            [['url'], 'string', 'max' => 150],
            [['class'], 'string', 'max' => 45],
            [['start', 'end'], 'string', 'max' => 30],
            [['status'], 'string', 'max' => 20],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['usuario_id' => 'id']],
            [['title'], 'exist', 'skipOnError' => true, 'targetClass' => Ambiente::className(), 'targetAttribute' => ['title' => 'id_ambiente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Numero de la reserva',
            'title' => 'Title',
            'body' => 'Descripción ',
            'type_event' => 'Type Event',
            'url' => 'Url',
            'class' => 'Class',
            'start' => 'Start',
            'end' => 'End',
            'inicio_normal' => 'Fecha de inicio',
            'final_normal' => 'Fecha fin',
            'status' => 'Estado',
            'limit_events' => 'Limit Events',
            'usuario_id' => 'Usuario Id Usuario',
        ];
    }

    /**
     * Gets query for [[DanoAmbientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDanoAmbientes()
    {
        return $this->hasMany(DanoAmbiente::className(), ['Reserva_id_reserva' => 'id']);
    }

    /**
     * Gets query for [[Titles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTitles()
    {
        return $this->hasMany(Ambiente::className(), ['id_ambiente' => 'title'])->viaTable('dano_ambiente', ['Reserva_id_reserva' => 'id']);
    }

    /**
     * Gets query for [[UsuarioIdUsuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_id']);
    }

    /**
     * Gets query for [[Title0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmbiente()
    {
        return $this->hasOne(Ambiente::className(), ['id_ambiente' => 'title']);
    }
    /**
     * Gets query for [[Title0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmbiente2()
    {
        return $this->hasMany(Ambiente::className(), ['id_ambiente' => 'title']);
    }
}
