<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "dano_ambiente".
 *
 * @property int $Id_dano_ambiente
 * @property string $Detalle_del_dano
 * @property string $Tipo_de_dano
 * @property string $Objetos_del_ambiente
 * @property int $Cantidad_objetos
 * @property int $title
 * @property int $Reserva_id_reserva
 *
 * @property Ambiente $title0
 * @property Reserva $reservaIdReserva
 */
class DanoAmbiente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dano_ambiente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Detalle_del_dano', 'Tipo_de_dano',   'Reserva_id_reserva'], 'required'],
            [[ 'Reserva_id_reserva'], 'integer'],
            [['Detalle_del_dano'], 'string', 'max' => 45],
            [['Tipo_de_dano'], 'string', 'max' => 200],
            [[ 'Reserva_id_reserva'], 'unique', 'targetAttribute' => [ 'Reserva_id_reserva']],
            [['Reserva_id_reserva'], 'exist', 'skipOnError' => true, 'targetClass' => Reserva::className(), 'targetAttribute' => ['Reserva_id_reserva' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id_dano_ambiente' => 'ID Daño',
            'Detalle_del_dano' => 'Detalle del daño ',
            'Tipo_de_dano' => 'Tipo de daño',
            'Reserva_id_reserva' => 'Reserva',
        ];
    }


    /**
     * Gets query for [[ReservaIdReserva]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReserva()
    {
        return $this->hasOne(Reserva::className(), ['id' => 'Reserva_id_reserva']);
    } 
}
