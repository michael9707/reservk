<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Partida;

/**
 * PartidaSearch represents the model behind the search form about `\frontend\models\Partida`.
 */
class PartidaSearch extends Partida
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_partida', 'velocidad', 'numero_jugadores', 'creado_por', 'actualizado_por','ganador'], 'integer'],
            [['valor_carton'], 'number'],
            [['creado_el', 'actualizado_el'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Partida::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_partida' => $this->id_partida,
            'velocidad' => $this->velocidad,
            'numero_jugadores' => $this->numero_jugadores,
            'valor_carton' => $this->valor_carton,
            'creado_por' => $this->creado_por,
            'creado_el' => $this->creado_el,
            'actualizado_por' => $this->actualizado_por,
            'actualizado_el' => $this->actualizado_el,
        ]);

        return $dataProvider;
    }
}
