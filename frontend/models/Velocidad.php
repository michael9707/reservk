<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "velocidad".
 *
 * @property int $id_velocidad
 * @property string $nombre_velocidad
 */
class Velocidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'velocidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre_velocidad'], 'required'],
            [['nombre_velocidad'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_velocidad' => 'Id Velocidad',
            'nombre_velocidad' => 'Nombre Velocidad',
        ];
    }
}
