<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "partida".
 *
 * @property int $id_partida
 * @property int $velocidad
 * @property int $numero_jugadores
 * @property float $valor_carton
 * @property int|null $ganador
 * @property int $creado_por
 * @property string $creado_el
 * @property int $actualizado_por
 * @property string $actualizado_el
 *
 * @property User $creadoPor
 * @property User $actualizadoPor
 * @property User $ganador0
 * @property Velocidad $velocidad0
 * @property PartidaJugadores[] $partidaJugadores
 * @property User[] $jugadors
 */
class Partida extends \common\models\MyActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partida';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['velocidad', 'numero_jugadores', 'valor_carton'], 'required'],
            [['velocidad', 'numero_jugadores', 'ganador', 'creado_por', 'actualizado_por'], 'integer'],
            [['valor_carton'], 'number'],
            [['creado_el', 'actualizado_el'], 'safe'],
            [['creado_por'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creado_por' => 'id']],
            [['actualizado_por'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['actualizado_por' => 'id']],
            [['ganador'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ganador' => 'id']],
            [['velocidad'], 'exist', 'skipOnError' => true, 'targetClass' => Velocidad::className(), 'targetAttribute' => ['velocidad' => 'id_velocidad']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_partida' => 'Id de Partida',
            'velocidad' => 'Velocidad',
            'numero_jugadores' => 'Número Jugadores',
            'valor_carton' => 'Valor Cartón',
            'ganador' => 'Ganador',
            'creado_por' => 'Creado Por',
            'creado_el' => 'Creado El',
            'actualizado_por' => 'Actualizado Por',
            'actualizado_el' => 'Actualizado El',
        ];
    }

    /**
     * Gets query for [[CreadoPor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreadoPor()
    {
        return $this->hasOne(User::className(), ['id' => 'creado_por']);
    }

    /**
     * Gets query for [[ActualizadoPor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getActualizadoPor()
    {
        return $this->hasOne(User::className(), ['id' => 'actualizado_por']);
    }

    /**
     * Gets query for [[Ganador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGanador0()
    {
        return $this->hasOne(User::className(), ['id' => 'ganador']);
    }

    /**
     * Gets query for [[Velocidad0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVelocidad0()
    {
        return $this->hasOne(Velocidad::className(), ['id_velocidad' => 'velocidad']);
    }

    /**
     * Gets query for [[PartidaJugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidaJugadores()
    {
        return $this->hasMany(PartidaJugadores::className(), ['id_partida' => 'id_partida']);
    }

    /**
     * Gets query for [[Jugadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadors()
    {
        return $this->hasMany(User::className(), ['id' => 'id_jugador'])->viaTable('partida_jugadores', ['id_partida' => 'id_partida']);
    }
}
