<?php

namespace frontend\controllers;

use DateTime;
use frontend\models\DanoAmbiente;
use Yii;
use frontend\models\Reserva;
use frontend\models\ReservaSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * ReservaController implements the CRUD actions for Reserva model.
 */
class ReservaController extends Controller
{
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'index', 'list','create',
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'list','create'
                        ],
                        'roles' => ['r-admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index','create'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reserva models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'main-admin';

        $searchModel = new ReservaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Reserva models.
     * @return mixed
     */
    public function actionList()
    {
        $this->layout = 'main-admin';

        $searchModel = new ReservaSearch();
        $dataProvider = $searchModel->searchAdmin(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Reserva model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = 'main-admin';

        if($this->isMineReserva($id)){
           echo Yii::$app->session->setFlash('danger', "No puede ver esta reserva.");
           return;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reserva model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'main-admin';

        $model = new Reserva();
        $countReservas = Reserva::find()->where(['usuario_id'=>Yii::$app->user->id])->count() == 0;


        if (!empty(Yii::$app->request->post())) {
            $request  = Yii::$app->request->post();
            $this->saveEvent($request, $model);
            return;
        }

        return $this->render('calendar', [
            'model' => $model,
            'count'=>$countReservas
        ]);
    }

    protected function saveEvent($request, $model)
    {
        header("Content-Type: application/json; charset=UTF-8");

        $data = ["status" => "error", "message" => "Ocurrió un error inesperado."];

        $user          = Yii::$app->user->id;
        $inicio        = $this->changeDate($request['from']);
        $final         = $this->changeDate($request['to']);
        $inicio_normal = $request['from'];
        $final_normal  = $request['to'];
        $title         = $request['title'];
        $body          = $request['event'];

        //Parse Fechas
        $parse_ini = DateTime::createFromFormat("d/m/Y H:i", $inicio_normal)->format("Y-m-d H:i");
        $parse_fin = DateTime::createFromFormat("d/m/Y H:i", $final_normal)->format("Y-m-d H:i");
        
        //Valido si la hora final es menor que la inicial
        if($parse_fin < $parse_ini ){
            echo json_encode(["status" => "error", "message" => "No es permitido que la fecha final sea menor que la fecha inicial"]);
            return;
        }
        //Valido si ya existen
        $validate_event = $this->validateDate($title, $parse_ini, $parse_fin);
        if (!empty($validate_event)) {
            echo json_encode(["status" => "error", "message" => "Las fechas establecidas tienen un evento asignado, con el nombre: <strong>" . $validate_event->ambiente->title . "</strong>"]);
            return;
        }
        if($this->validateDanus($title, $user)){
            echo json_encode(["status" => "error", "message" => "No puede reservar. Tiene tres o más daños reportados."]);
            return;
        }

        $model->title = $title;
        $model->body  = $body;
        $model->class = 'event-especial';
        $model->start = (string) $inicio;
        $model->end   = (string) $final;
        $model->type_event    = "event-info";
        $model->inicio_normal = $parse_ini;
        $model->final_normal  = $parse_fin;
        $model->status        = 'activo';
        $model->limit_events  = '1';
        $model->usuario_id = $user;
        $model->url = "1";

        if (!$model->validate()) {
            echo json_encode(["status" => "error", "message" => "Ocurrió un error al registrar la información, valida tu sesión o el formulario."]);
            return;
        }

        $model->save();
        $link = Yii::$app->request->baseUrl . "/descripcion_evento.php?id=" . $model->id;

        $model->url =  $link;

        if ($model->save()) {
            $data = ["status" => "ok", "message" => "El eventó se guardó correctamente"];
        }


        echo json_encode($data);
    }

 

    /**
     * Updates an existing Reserva model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);

        if (!empty($model) and !empty(Yii::$app->request->post())) {

            $request = Yii::$app->request->post();
            $data    = ["status" => "error", "message" => "Ocurrió un error inesperado."];
            $tipo    = $request['type_e'];

            if ($tipo == 'eliminar') {
                $this->modifyStatus($model);
                return;
            }

            $inicio        = $this->changeDate($request['from']);
            $final         = $this->changeDate($request['to']);
            $inicio_normal = $request['from'];
            $final_normal  = $request['to'];

            //Parse Fechas
            $parse_ini = DateTime::createFromFormat("d/m/Y H:i", $inicio_normal)->format("Y-m-d H:i");
            $parse_fin = DateTime::createFromFormat("d/m/Y H:i", $final_normal)->format("Y-m-d H:i");

            //Valido si ya existen
            $validate_event = $this->validateDate($model->title, $parse_ini, $parse_fin, $model->id);
            if (!empty($validate_event)) {
                echo json_encode(["status" => "error", "message" => "Las fechas establecidas tienen un evento asignado, con el nombre: <strong>" . $validate_event->ambiente->title . "</strong>"]);
                return;
            }

            $getLimit = $this->validateLimit($model);
            if (!$this->validateLimit($model)) {
                echo json_encode(["status" => "error", "message" => "Excedió el número de modificaciones en la reserva."]);
                return;
            }

            $model->inicio_normal = $parse_ini;
            $model->final_normal  = $parse_fin;
            $model->start = (string) $inicio;
            $model->end = (string) $final;
            $model->limit_events = $getLimit;

            if ($model->save()) {
                $data = ["status" => "ok", "message" => "Evento re agendado correctamente."];
            } else {
                var_dump($model->errors);
                $data = ["status" => "error", "message" => "El evento no se pudo reagendar."];
            }

            echo json_encode($data);
            return;

            //return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function modifyStatus($model)
    {
        $model->status = 'cancelado';
        $model->class  = 'cancel-event';

        if(!$this->validateHour($model)){
            echo json_encode(["status" => "error", "message" => "El evento no se puede cancelar una hora antes de su inicialización."]);
            return;
        }

        if ($model->save()) {
            echo json_encode(["status" => "ok", "message" => "Evento cancelado."]);
            
        } else {
            echo json_encode(["status" => "error", "message" => "El evento no se pudo cancelar."]);
        }
    }

    protected function validateHour($model){
        date_default_timezone_set('America/Bogota');

        $get_final = new DateTime($model->inicio_normal);
        $get_now   = new DateTime(date('m/d/Y h:i:s a'));
        $diff  = $get_now->diff($get_final);
        $hours = $diff->h;
        $hours = $hours + ($diff->days*24);

        if(($hours <=1 and $diff->invert==0)  or $diff->invert==1){
            return false;
        }
        return true;
    }

    /**
     * Obtiene los eventos a mostrar en JSON
     * 
     */
    public function actionEvents()
    {
        header("Content-Type: application/json; charset=UTF-8");

        if (Yii::$app->user->can('r-admin')) {
            $get_events = Reserva::find()->where('final_normal >= NOW()');
        } else {
            $get_events = Reserva::find()->where(['usuario_id' => Yii::$app->user->id])->andWhere('final_normal >= NOW()');
        }

        if (!empty($get_events)) {

            $datos = [];

            foreach ($get_events->all() as $i => $event) {
                $datos[$i]["title"] = $event->ambiente->title . " - " . strtoupper($event->status);
                $datos[$i]["body"]  = $event->body;
                $datos[$i]["class"] = $event->ambiente->color . ' ' . $event->class;
                $datos[$i]["url"]   = Url::toRoute(['/reserva/view', 'id' => $event->id]);
                $datos[$i]["start"] = $event->start;
                $datos[$i]["end"]   = $event->end;
            }

            echo json_encode(["success" => 1, "result" => $datos]);
        } else {
            echo json_encode(["error" => 1, "result" => "No hay datos."]);
        }
    }

    protected function validateDate($title, $parse_ini, $parse_fin, $id = NULL)
    {
        return Reserva::find()->where(['reserva.title' => $title, 'status' => 'activo'])->andWhere("('$parse_ini' BETWEEN inicio_normal and final_normal or '$parse_fin' BETWEEN inicio_normal and final_normal)")->andFilterWhere(['<>', 'id', $id])->one();
    }

    protected function validateLimit($model)
    {
        $limits = (int) $model->limit_events;
        return ($limits >= 3) ? false :  ++$limits;
    }

    protected function changeDate($fecha)
    {
        return strtotime(substr($fecha, 6, 4) . "-" . substr($fecha, 3, 2) . "-" . substr($fecha, 0, 2) . " " . substr($fecha, 10, 6)) * 1000;
    }

    protected function validateDanus($title,$user){
        $get_dan = DanoAmbiente::find()->innerJoinWith('reserva a')->where(['a.title'=>$title,'a.usuario_id'=>$user])->count();
        return $get_dan >= 3;
    }

    protected function isMineReserva($id){
        $get_reserva = Reserva::find()->where(['id'=>$id,'usuario_id'=>Yii::$app->user->id])->one();
        return empty($get_reserva) && !Yii::$app->user->can('r-admin');
    }
    /**
     * Deletes an existing Reserva model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reserva model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reserva the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reserva::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
