<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */
?>
<div class="user-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'surname',
            'id_number',
            // 'username',
            'email:email',
            // 'estado',
            // 'created_at',
        ],
    ]) ?>

</div>
