<?php

use backend\models\AuthAssignment;
use kartik\grid\GridView;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'value'=>function($data){
            return $data->name. ' '.$data->surname;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'id_number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'username',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'header'=>'Rol',
        'value'=>function($data){
            return empty(AuthAssignment::find()->where(['user_id'=>$data->id])->one()->item_name) ? 'Funcionario' : 'Administrador';
        }
    ],

    [
        'label' => 'Estado',
        'attribute' => 'estado',
        'value' => function ($data) {
            switch ($data->estado) {
                case 0:
                    return '<label class="label label-warning">Inactivo</label>';
                    break;
                case 1:
                    return '<label class="label label-success">Activo</label>';
                    break;
            }
        },
        'format' => 'raw',
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => [
            0 => 'Inactivo',
            1 => 'Activo',
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => '', 'style' => 'width: 6.68%;'],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'¿Está seguro?',
<<<<<<< HEAD
                          'data-confirm-message'=>'¿Está seguro que desea eliminar este usuario?'], 
=======
                          'data-confirm-message'=>'¿Está seguro que desea eliminar este registro de daño?'], 
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
    ],

];   