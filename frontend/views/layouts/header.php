<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<head>
<link rel="shortcut icon" href="../assets/images/favicon.png">
</head>
<header class="main-header" style="background-color: rgba(0, 0, 0, 0.7); color: #fff;">

    <?=
        Html::a('<span class="logo-mini">' .
            '<img src="' . Yii::$app->request->baseUrl . '/../assets/images/logo+sena.png' . '" class="img-responsive" style="max-height: 45px !important;">'
            . '</span><span class="logo-lg">'
            . '<img src="' . Yii::$app->request->baseUrl . '/../assets/images/sena+logo.png' . '" class="img-responsive" >' .
            '</span>', Yii::$app->homeUrl, ['class' => 'logo'])
    ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user-circle fa-4" style="color:white;"></i>
                        <span style="color:white;"  class="hidden-xs"><?= Yii::$app->user->identity->name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <i class="fa fa-user-circle fa-4" style="font-size: 50px;color:#F78536;margin-left:5px;"></i>

                            <p style="color:#000;">
                                <?= Yii::$app->user->identity->name ?>
                                <small>Creado desde <?= Yii::$app->user->identity->created_at ?></small>
                            </p>
                        </li>
                        <li class="user-footer">
                         
                            <div class="pull-right">
                                <?=
                                    Html::a(
                                        'Salir',
                                        ['/site/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                    )
                                ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>