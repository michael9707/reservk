<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <i class="fa fa-user-circle fa-4" style="font-size: 40px;color:#F78536;margin-left:5px;"></i>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->name ?></p>

                <a ><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?php


        echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Menú', 'options' => ['class' => 'header']],
                    ['label' => 'Página principal', 'icon' => 'home', 'url' => Yii::$app->homeUrl, 'target' => '_blank'],
                    ['label' => 'Crear Reserva', 'icon' => 'calendar-plus-o', 'url' => ['/reserva/create'], 'target' => '_blank'],
                    ['label' => 'Mis Reservas', 'icon' => 'calendar', 'url' => ['/reserva'], 'target' => '_blank'],
                    ['label' => 'Daños de Ambiente', 'icon' => 'archive', 'url' => ['/dano-ambiente'], 'target' => '_blank'],

                    [
                        'label' => 'Administrador',
                        'icon' => 'user-o',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('r-admin'),
                        'items' => [
                            ['label' => 'Daños de Ambiente', 'icon' => 'archive', 'url' => ['/dano-ambiente/list'], 'target' => '_blank'],
                            ['label' => 'Lista de Reservas', 'icon' => 'calendar', 'url' => ['/reserva/list'], 'target' => '_blank'],
                            ['label' => 'Usuarios', 'icon' => 'address-book', 'url' => ['/user'], 'target' => '_blank'],
                        ]
                    ],

                ],
            ]
        )
        ?>

    </section>

</aside>