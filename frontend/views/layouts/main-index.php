<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="../assets/images/favicon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  

    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/bootstrap-4.1.2/bootstrap.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/OwlCarousel2-2.3.4/owl.carousel.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/OwlCarousel2-2.3.4/owl.theme.default.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/OwlCarousel2-2.3.4/animate.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/jquery-datepicker/jquery-ui.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/colorbox/colorbox.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/main_styles.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/responsive.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/booking.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/booking_responsive.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/contact.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/contact_responsive.css']); ?>

    <title><?= Html::encode($this->title) ?></title>
    <?php $this->registerCsrfMetaTags()?>

    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>
    <section>
        <div class="wrap">

            <header class="header">
                <div class="header_content d-flex flex-row align-items-center justify-content-start">

                    <div> <input type="image" src="<?= Yii::$app->request->baseUrl . '/../assets/images/logo+sena.png'; ?>" alt="" style="margin-left:-14px; height: 35px; "> </div>

                    <div class="ml-auto d-flex flex-row align-items-center justify-content-start">
                        <nav class="main_nav">
                            <ul class="d-flex flex-row align-items-start justify-content-start">
                                <li class="active"><a href="<?= Url::to(['site/']) ?>">Inicio</a></li>
                                <li><a href="<?= Url::to(['site/about']) ?>">Condiciones de reserva </a></li>
                                <li><a href="<?= Url::to(['site/contact']) ?>">Contáctenos</a></li>
                            </ul>
                        </nav>
                        <?php if (Yii::$app->user->isGuest) : ?>
                            <div class="book_button"><a href="<?= Url::to(['site/login/']) ?>">Iniciar Sesión</a></div>
                        <?php else : ?>
                            <div class="book_button"><a href="<?= Url::to(['/reserva/create']) ?>">Ingresar</a></div>
                        <?php endif; ?>
                        <!-- Hamburger Menu -->
                        <div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
                    </div>
                </div>
            </header>
            <div class="menu trans_400 d-flex flex-column align-items-end justify-content-start">
                <div class="menu_close"><i class="fa fa-times" aria-hidden="true"></i></div>
                <div class="menu_content">
                    <nav class="menu_nav text-right">
                        <ul>
                            <li><a href="<?= Url::to(['site/']) ?>">Inicio</a></li>
                            <li><a href="<?= Url::to(['site/about']) ?>">Condiciones de reserva </a></li>

                            <li><a href="<?= Url::to(['site/contact']) ?>">Contáctenos</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="menu_extra">
                    <?php if (Yii::$app->user->isGuest) : ?>
                        <div class="menu_book text-right"><a href="<?= Url::to(['site/login/']) ?>">Iniciar Sesión</a></div>
                    <?php else : ?>
                        <div class="menu_book text-right"><a href="<?= Url::to(['reserva/create']) ?>">Ingresar</a></div>
                    <?php endif; ?>
                </div>
            </div>

<<<<<<< HEAD
=======

>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
            <div>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
<<<<<<< HEAD
           
=======
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
        </div>
    </section>
    <?php $this->endBody() ?>
</body>

</html>

<!-- <script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/js/jquery-3.3.1.min.js"></script>
 -->
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/styles/bootstrap-4.1.2/popper.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/greensock/TweenMax.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/greensock/TimelineMax.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/greensock/animation.gsap.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/OwlCarousel2-2.3.4/owl.carousel.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/easing/easing.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/progressbar/progressbar.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/parallax-js-master/parallax.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/jquery-datepicker/jquery-ui.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/js/custom.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/js/booking.js"></script>
<?php $this->endPage() ?>