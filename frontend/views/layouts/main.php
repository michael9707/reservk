<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="../assets/images/favicon.png">
<<<<<<< HEAD
    <link rel="shortcut icon" href="../../assets/images/favicon.png">
=======
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/main_styles.css']); ?>
<<<<<<< HEAD
=======
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/responsive.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/booking.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/booking_responsive.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/contact.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/contact_responsive.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/calendar.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/Calendario/bootstrap-datetimepicker.min.css']); ?>
    <?= $this->registerLinkTag(['rel' => 'stylesheet', 'type' => 'text/css', 'href' =>  Yii::$app->request->baseUrl . '/../assets/styles/calendario.css']); ?>

    <title><?= Html::encode($this->title) ?></title>
>>>>>>> 5aa45dce5abc69c8d67fa679d76bfd8dfeb0f115


    <?php $this->head() ?>
</head>
<body>
<<<<<<< HEAD
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
=======
<<<<<<< HEAD

    <?php $this->beginBody() ?>
    <div>
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
=======
    <?php $this->beginBody() ?>


<!-- 
    <div class="wrap">

        <header style="background: rgba(0, 0, 0, 0.7) !important;">
            <div class="header_content d-flex flex-row align-items-center justify-content-start">

                <div> <input type="image" src="<?= Yii::$app->request->baseUrl . '/../assets/images/logo.png'; ?>" alt="" style="margin-left:-14px; height: 35px; "> </div>

                <div class="ml-auto d-flex flex-row align-items-center justify-content-start">
                    <nav class="main_nav">
                        <ul class="d-flex flex-row align-items-start justify-content-start">
                            <li class="active"><a href="<?= Url::to(['site/']) ?>">Inicio</a></li>
                            <li><a href="<?= Url::to(['site/about']) ?>">Condiciones </a></li>
                            <li><a href="<?= Url::to(['site/contact']) ?>">Contactenos</a></li>
                        </ul>
                    </nav>
                    <div class="book_button"><a href="<?= Url::to(['site/login/']) ?>">Iniciar sesion</a></div>


                   
                    <div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
                </div>
            </div>
        </header >
        <div class="menu trans_400 d-flex flex-column align-items-end justify-content-start">
            <div class="menu_close"><i class="fa fa-times" aria-hidden="true"></i></div>
            <div class="menu_content">
                <nav class="menu_nav text-right">
                    <ul>
                        <li><a href="<?= Url::to(['site/']) ?>">Inicio</a></li>
                        <li><a href="<?= Url::to(['site/about']) ?>">Condiciones</a></li>

                        <li><a href="<?= Url::to(['site/contact']) ?>">Contactenos</a></li>
                    </ul>
                </nav>
            </div>
            <div class="menu_extra">
                <div class="menu_book text-right"><a href="<?= Url::to(['site/login/']) ?>">Iniciar sesion</a></div>

            </div>
        </div> -->


>>>>>>> 5aa45dce5abc69c8d67fa679d76bfd8dfeb0f115

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<<<<<<< HEAD
<?php $this->endBody() ?>
</body>
=======
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
    <?php $this->endBody() ?>
</body>

<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/es-ES.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/moment.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/bootstrap.min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/bootstrap-datetimepicker.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/bootstrap-datetimepicker.es.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/underscore-min.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/calendar.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/jscalendario/ojo.js"></script>
<<<<<<< HEAD
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/js/ojo.js"></script>
<script src="<?= Yii::$app->request->baseUrl . '/..'; ?>/assets/js/btnlogin.js"></script>
<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>

=======
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
>>>>>>> 5aa45dce5abc69c8d67fa679d76bfd8dfeb0f115
</html>
<?php $this->endPage() ?>
