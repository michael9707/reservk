<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reserva */

$this->title = 'Crear Reserva';
$this->params['breadcrumbs'][] = ['label' => 'Reservas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<title>RESERVK | Reservas</title>
</head>
<div class="reserva-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
