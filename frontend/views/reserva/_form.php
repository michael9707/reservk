<?php

use frontend\models\Ambiente;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Reserva */
/* @var $form yii\widgets\ActiveForm */
?>
<head>
<title>RESERVK | Información  del evento</title>
</head>
<div class="reserva-form" style="margin-top: 40px;">
    <div class="container">
        <div class="row" style="margin-bottom:40px;">
            <div class="col-sm-3 card">
                <div class="col-sm-12">
                    <label class="pol"> GYM </label><span class="circle darkgren"></span>
                </div>
                <div class="col-sm-12">
                    <label class="pol"> AUDITORIO </label><span class="circle black"></span>

                </div>
                <div class="col-sm-12">
                    <label class="pol"> AUDIO VISUALES </label><span class="circle blue"></span>

                </div>
                <div class="col-sm-12">
                    <label class="pol"> CANCELADO</label><span class="circle red"></span>
                </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-4" style="text-align: center;">
                <div class="page-header">
                    <strong>
                        <h2 style="font-weight:bold;"></h2>
                    </strong>
                </div>
                <div>
                    <a href="calendario.php">
                        <button class="btn btn-success">
                            Actualizar
                        </button>
                    </a>
                    <button class="btn btn-warning" data-toggle='modal' data-target='#add_evento'>
                        + Añadir un nuveo evento
                    </button>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="buttons-group" style="margin-top: 40px;">
                    <div class="btn-group">
                        <button class="btn btn-warning" data-calendar-nav="prev">
                            << Anterior</button> <button class="btn btn-primary" data-calendar-nav="today">Hoy
                        </button>
                        <button class="btn btn-warning" data-calendar-nav="next">Siguiente >></button>
                    </div><br><br>
                    <div class="btn-group">
                        <button class="btn btn-info active" data-calendar-view="month">Mes</button>
                        <button class="btn btn-info" data-calendar-view="week">Semana</button>

                    </div>
                </div>
            </div>

        </div>
        <div class="row" style="color: black;background-color: white;">
            <div id="calendar"></div>
            <br><br>
        </div>
       

    </div>
    <div class="modal fade" id="add_evento" tabindex="-1" role="dialog" >
            <form action="../reserva/create" method="post" id="create_event">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel">Agregar nuevo evento</h4>
                        </div>
                        <div class="modal-body">

                            <label for="from">Inicio</label>
                            <div class='input-group date' id='from'>
                                <input type='text' id="from" name="from" class="form-control" readonly />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </div>

                            <br>

                            <label for="to">Final</label>
                            <div class='input-group date' id='to'>
                                <input type='text' name="to" id="to" class="form-control" readonly />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </div>


                            <br>
                            <label for="title">Ambiente</label>
                            <select name="title" class="form-control" id="title" required>
                                <?php foreach (Ambiente::find()->all() as $ambiente) : ?>
                                    <option value="<?= $ambiente->id_ambiente ?>"><?= $ambiente->title; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <br>
                            <label for="body">Evento</label>
                            <textarea id="body" name="event" required class="form-control" rows="3" placeholder="Descripcion"></textarea>

                        </div>
                        <div class="modal-footer">
                            <button  type="submit" class="btn btn-success"><i class="fa fa-check" style="color: white;" ></i> Agregar</button>
                            <button style="color: white;" type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>

                            <div class="alert alert-danger" role="alert">

                            </div>
                            <div class="alert alert-success" role="alert">
                            </div>
                        </div>


                    </div>
                </div>
            </form>
        </div>
    <script>
        var url_ = "<?= Yii::$app->request->baseUrl . '/..'; ?>";
    </script>
    <?php
    $this->registerJsFile(
        '@web/../assets/jscalendario/jquery.min.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
    $this->registerJsFile(
        '@web/../assets/js/calendario.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );

    ?>