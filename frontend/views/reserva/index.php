<?php

use frontend\models\Ambiente;
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ReservaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Listas de Reservas';
$this->params['breadcrumbs'][] = $this->title;
?>
<head>
<title>RESERVK | Lista de reservas</title>
</head>
<br>
<div class="reserva-index">


    <?= GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => [
            'id',

            [
                'label'=>'Ambiente',
                'attribute' => 'title',
                'value' => function ($data) {
                    return $data->ambiente->title;
                },
                'filter' => ArrayHelper::map(Ambiente::find()->all(), 'id_ambiente', 'title'),
                'class' => '\kartik\grid\DataColumn',
                'vAlign' => GridView::ALIGN_MIDDLE,
                'filterType' => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'options' => ['placeholder' => 'Seleccionar...'],
                    'pluginOptions' => [
                        'allowClear' => TRUE
                    ],
                ],
            ],
            
           
            'body:ntext',
<<<<<<< HEAD

=======
            // 'type_event',
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
            'status',
   
            'inicio_normal',
            'final_normal',
       
            [
                'class' => 'kartik\grid\ActionColumn',
                'header'=>'Ver',
                'template'=>'{view}',
                'dropdown' => false,
                'vAlign'=>'middle',
                'urlCreator' => function($action, $model, $key, $index) { 
                        return Url::to([$action,'id'=>$key]);
                },
                'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
                'updateOptions'=>['role'=>'modal-remote','title'=>'Actualizar', 'data-toggle'=>'tooltip'],
                'deleteOptions'=>['role'=>'modal-remote','title'=>'Eliminar', 
                                  'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                  'data-request-method'=>'post',
                                  'data-toggle'=>'tooltip',
                                  'data-confirm-title'=>'¿Está seguro?',
                                  'data-confirm-message'=>'¿Está seguro que desea eliminar este registro de daño?'], 
            ],
        ],
      


        'toolbar' => [
            [
                'content' =>
                Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    [''],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset Grid']
                ) .
                    '{toggleData}' .
                    '{export}'
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'panel' => [
            'type' => 'primary',
            'heading' => '<i class="glyphicon glyphicon-list"></i> Lista de Reservas',
            'before' => '<em>*Módulo de las reservas de los ambientes.</em>',

            '<div class="clearfix"></div>',
        ] ,
        
    ]) ?>
</div>