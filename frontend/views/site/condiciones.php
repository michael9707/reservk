<?php

<<<<<<< HEAD
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reservk | Terminos y condiciones';
?>

<head>


<link rel="stylesheet" href="../../assets/styles/condiciones.css">
</head>

<body>
	<section>
<h1 class="title" >Terminos y condiciones</h1>

<h4>
<h3 class="subti" >A. Introducción</h3>

<p class="sub" >1. La privacidad de los visitantes de nuestro sitio web es muy importante y estamos comprometidos a protegerla. Esta política explica qué haremos con su información personal.
Cuando usted nos visita por primera vez y da su consentimiento para el uso que le damos a las cookies, de conformidad con los términos de esta política, usted nos da permiso para usar cookies cada vez que visita nuestro sitio web.</p> 
<br>
<br>
<p class="sub" >2. Cuando usted nos visita por primera vez y da su consentimiento para el uso que le damos a las cookies, de conformidad con los términos de esta política, usted nos da permiso para usar cookies cada vez que visita nuestro sitio web.</p>  

<br>
<h3 class="subti" >B. Crédito</h3>
<p class="sub" >Este documento fue creado usando una plantilla de SEQ Legal (seqlegal.com). </p>
<br>
<h3 class="subti" >C. Recopilar información personal</h3>

<p class="sub">Los siguientes tipos de información personal pueden ser recopilados, almacenados y usados:</p> 

<p class="sub"  >  1. información sobre su ordenador, incluyendo su dirección IP, ubicación geográfica, tipo y versión de navegador, y sistema operativo;</p>

<p class="sub"  >2. información sobre sus visitas y uso de este sitio web, incluidas las fuentes de referencia, duración de la visita, visitas de la página y rutas de navegación del sitio web;
 </p>

<p class="sub">3. información que introduzca al crear una cuenta en nuestro sitio web, como la dirección de correo electrónico; </p>
<p class="sub">4. información que introduzca al crear un perfil en nuestro sitio web, por ejemplo, su nombre, foto de perfil, género, cumpleaños, estado civil, intereses y pasatiempos, información sobre formación y empleo;
 </p>
<p class="sub">5. información que introduzca para suscribirse a nuestros correos y boletines, como su nombre y dirección de correo electrónico;
 </p>
<p class="sub">6. información que introduzca mientras usa los servicios en nuestro sitio web;
 </p>
<p class="sub">7. información que se genera mientras usa nuestro sitio web, incluido cuándo, qué tan a menudo y bajo qué circunstancias lo use;
 </p>
<p class="sub"> 8. información sobre cualquier aspecto relacionado con su compra, servicios que use o transacciones que haga a través del sitio web, incluido su nombre, dirección, número telefónico, dirección de correo electrónico e información de tarjeta de crédito;
</p>
<p class="sub"> 9. información que publique en nuestro sitio web con la intención de que sea publicada en Internet, lo que incluye su nombre de usuario, fotos de perfil y contenido de sus publicaciones;
</p>
<p class="sub">10. información contenida en cualquiera de las comunicaciones que nos envía a través de correo electrónico o de nuestro sitio web, incluido el contenido de la comunicación y metadatos;
 </p>

<p class="sub" > 11. cualquier otra información personal que nos envíe. </p>

<p class="sub" >Antes de divulgarnos la información personal de otra persona, primero debe obtener el consentimiento de esa persona, tanto para la divulgación como para el procesamiento de esa información personal de acuerdo con esta política;
  </p>
  <br>
<h3 class="subti" >D. Usar su información personal</h3>

<p class="sub" > La información personal que nos envíe a través de nuestro sitio web será usada con los fines especificados en esta política o en las páginas relevantes del sitio web. Podemos usar su información personal para los siguientes fines:
 </p>
<p class="sub" > 1. administrar nuestro sitio web y negocio;
 </p>
<p class="sub" > 2. activar el uso de servicios disponibles en nuestro sitio web;
 </p>
<p class="sub" >3. enviarle artículos comprados a través de nuestro sitio web;
  </p>
<p class="sub" > 4. suministrar servicios comprados a través de nuestro sitio web;
 </p>
<p class="sub" >5. enviar extractos, facturas y recordatorios de pago y recopilar datos suyos;
  </p>
<p class="sub" > 6. enviar comunicaciones comerciales (no de marketing);
 </p>
<p class="sub" >7. enviar notificaciones de correo electrónico que ha solicitado específicamente;
  </p>
<p class="sub" >8. enviar un boletín de correo electrónico, si usted lo ha solicitado (puede informarnos en cualquier momento si no desea seguir suscrito al boletín);
  </p>

<p class="sub" >9. enviar comunicaciones de marketing relacionadas con nuestro negocio, o los negocios de terceros cuidadosamente seleccionados, que consideramos que serán de su interés, por correo postal o donde haya aceptado específicamente a esto, por correo electrónico o tecnologías similares (nos puede informar en cualquier momento si no desea seguir recibiendo las comunicaciones de marketing);
  </p>
<p class="sub" >10. dar información estadística a terceros sobre nuestros usuarios (pero esos terceros no podrán identificar a ningún usuario individual con esa información):
  </p>
<p class="sub" >11. dar respuesta a las preguntas y quejas suyas o sobre usted, relacionadas con nuestro sitio web;
  </p>
<p class="sub" >12. mantener protegido el sitio web y evitar el fraude;
  </p>
<p class="sub" > 13. verificar el cumplimiento de los términos y condiciones que rigen sobre el uso de nuestro sitio web (incluido la monitorización de mensajes privados a través del servicio de mensajería privada de nuestro sitio web); 
 </p>
<p class="sub" >14. y otros usos.
  </p>
<p class="sub" > Si usted envía información personal para su publicación en nuestro sitio web, publicaremos y usaremos esa información de conformidad con la licencia que usted nos confiere.
Sus ajustes de privacidad pueden usarse para limitar la publicación de nuestra información en nuestro sitio web y puede ajustarse usando controles de privacidad en el sitio web.
Sin su consentimiento explícito no proporcionaremos su información personal a ningún tercero para su marketing directo o el de otro tercero. </p>

<br>
<h3 class="subti" >E. Divulgar información personal</h3>

<p class="sub" >Podremos divulgar su información personal a cualquiera de nuestros empleados, oficiales, aseguradores, consejeros profesionales, agentes, proveedores o contratistas, como sea razonablemente necesario para los fines descritos en esta política.
 </p>
<p class="sub" >Podremos divulgar su información personal a cualquier miembro de nuestro grupo de empresas (esto incluye subsidiarios, nuestro grupo y todas sus subsidiarias), como sea razonablemente necesario para los fines descritos en esta política.
 </p>
<p class="sub" >Podemos divulgar su información personal:
 </p>
<p class="sub" >1. hasta lo que sea requerido por la ley;
 </p>
<p class="sub" >2. en relación con cualquier procedimiento legal actual o prospectivo;
 </p>
<p class="sub" > 3. para establecer, ejercer o defender nuestros derechos legales (incluido proporcionar información personal a otros con el fin de evitar fraudes y reducir el riesgo crediticio);
</p>

<p class="sub">4. al comprador (o comprador prospectivo) de cualquier negocio o activo que estemos vendiendo o estemos contemplando vender;  </p>
<p class="sub">5. a cualquier persona que creamos razonablemente que podrá aplicar a una corte o a otra autoridad competente para solicitar la divulgación de esa información personal, y que, bajo nuestra opinión razonable, dicha corte o autoridad tendrá una probabilidad razonable de ordenar la divulgación de dicha información personal.
 </p>
<p class="sub">Con excepción de lo establecido por la ley, no proporcionaremos su información personal a terceros.</p>
<br>
<h3 class="subti" >F. International data transfers</h3>
<p class="sub">1. La información que recopilamos puede ser almacenada, procesada y transferida entre cualquiera de los países en que operamos, con el fin de permitirnos usar la información de conformidad con esta política.
  </p>
<p class="sub">2. La información que recopilamos puede ser transferida a los siguientes países donde no tenemos leyes de protección de datos equivalentes a las vigentes en el Espacio Económico Europeo: Estados Unidos de América, Rusia, Japón, China e India.
  </p>
<p class="sub">3. La información personal que publique en nuestro sitio web o envíe para su publicación en nuestro sitio web, puede estar disponible a través de Internet, en todo el mundo. No podemos evitar el uso o mal uso de dicha información por parte de otros.
  </p>
<p class="sub">4. Usted acepta explícitamente a las transferencias de información personal descritas en esta sección F.
  </p>
  <br>
<h3 class="subti">G. Conservar información personal</h3>
<p class="sub">1. Esta sección G especifica nuestras políticas y procedimientos de conservación de información personal, diseñadas para ayudar a garantizar que cumplimos con nuestras obligaciones legales con respecto a la conservación y eliminación de información personal.
  </p>
<p class="sub">2. La información personal que procesamos para cualquier fin o fines no debe ser almacenada más tiempo de lo necesario para ese fin o fines.
  </p>
<p class="sub">3. A pesar de las otras cláusulas de esta sección G, conservaremos los documentos (incluidos documentos electrónicos) que contengan datos personales:
        en medida de lo que sea requerido por la ley;  </p>
<p class="sub">
4. si creemos que los documentos pueden ser relevantes para cualquier procedimiento legal actual o prospectivo; y
        para establecer, ejercer o defender nuestros derechos legales (incluido proporcionar información personal a otros con el fin de evitar fraudes y reducir el riesgo crediticio).
  </p>
  <br>
<h3 class="subti">H. Seguridad de su información personal</h3>
<p class="sub">1. Tomaremos precauciones razonables técnicas y organizacionales para evitar la pérdida, mal uso o alteración de su información personal.  </p>
<p class="sub"> 2. Almacenaremos toda la información personal que nos dé en nuestros servidores seguros (protegidos por contraseña y cortafuegos).
 </p>
<p class="sub">3. Todas las transacciones financieras electrónicas realizadas a través de nuestro sitio web estarán protegidas por tecnología de cifrado.
  </p>
<p class="sub">4. Usted acepta que la transmisión de información en Internet es inherentemente insegura y que no podemos garantizar la seguridad de los datos enviados a través de Internet.
  </p>
<p class="sub">5. Usted es responsable de mantener de forma confidencial la contraseña que use para acceder a nuestro sitio web, y nosotros no le pediremos su contraseña (exceptuando para iniciar sesión en nuestro sitio web).
  </p>
  <br>
<h3 class="subti">I. Enmiendas</h3>
<p class="sub">	Es posible que actualicemos esta política de vez en cuando al publicar una nueva versión en nuestro sitio web. Usted debe comprobar ocasionalmente esta página para asegurarse de que entiende cualquier cambio a esta política. Es posible que le notifiquemos de cambios a esta política a través de correo electrónico o a través del sistema de mensajería privada de nuestro sitio web.
  </p>
  <br>	
<h3 class="subti">J. Sus derechos</h3>

<p class="sub" >Usted puede solicitarnos que le proporcionemos cualquier información personal que tengamos de usted; la entrega de dicha información estará sujeta a lo siguiente:
 </p>
<p class="sub" >1. el pago de una tarifa de {INTRODUCE LA TARIFA SI APLICA}; y
 </p>
<p class="sub" >2. que usted presenté evidencia apropiada sobre su identidad ({AJUSTA EL TEXTO PARA REFLEJAR TU POLÍTICA para este fin, usualmente aceptaremos una fotocopia del pasaporte certificada por un notario y una copia original de una factura que muestre su dirección actual}).
  </p>
<p class="sub">Podemos retener información personal que usted nos solicite en la medida de lo permitido por la ley. </p>

<p class="sub">Usted puede solicitarnos en cualquier momento que no procesemos su información personal para fines de marketing. </p>
<p class="sub">	En la práctica, usted usualmente aceptará explícitamente de antemano a que usemos su información personal para fines de marketing, o le daremos la oportunidad de que decida que no se use su información personal para fines de marketing.		
 </p>

 <br>
<h3 class="subti">K. Sitios web de terceros</h3>
<p class="sub">	Nuestro sitio web incluye hiperenlaces a, e información de, sitios web de terceros. No tenemos control ni somos responsables por las políticas de privacidad y prácticas de terceros.
 </p>

 <br>	
<h3 class="subti">	L. Actualizar información</h3>
	
<p class="sub">Por favor, infórmenos si la información personal que tenemos de usted necesita ser corregida o actualizada. </p>

<br>

<h3 class="subti">	M. Cookies</h3>
<p class="sub">Nuestro sitio web usa cookies. Una cookie es un fichero que contiene un identificador (una cadena de letras y números) que se envía desde un servidor web a un navegador web y se almacena en el navegador. El identificador es enviado de vuelta al servidor cada vez que el navegador solicita una página del servidor. Las cookies pueden ser “persistentes” o de “sesión”. Una cookie persistente será almacenada por una navegador web y permanecerá válida hasta su fecha de caducidad, a menos que el usuario la elimine antes de la fecha de caducidad. Una cookie de sesión caducará al final de la sesión del usuario, cuando se cierre el navegador. Las cookies típicamente no contienen ninguna información que identifique personalmente al usuario sino información personal que almacenamos sobre usted que puede relacionarse con la información almacenada y obtenida de las cookies. {SELECCIONA LA FORMA DE DECIRLO MÁS PRECISA Solo usamos cookies de sesión / Solo usamos cookies persistentes / Solo usamos cookies de sesión y persistentes en nuestro sitio web.}
 </p>
<p class="sub">1. Los nombres de las cookies que podemos usar en nuestro sitio web y los fines para las que son usadas, están especificadas a continuación:
 </p>
<p class="sub">1. usamos Google Analytics y Adwords en nuestro sitio web para reconocer un ordenador cuando un usuario {INCLUYE TODO PARA LO QUE SON USADAS LAS COOKIES EN TU SITIO visita el sitio web / monitorizar usuarios mientras navegan por el sitio web / permitir el uso de un carrito de compras en el sitio web / mejorar la usabilidad del sitio / analizar el uso del sitio web / administrar el sitio web / evitar fraude y mejorar la seguridad del sitio web / personalizar el sitio web para cada usuario / personalizar los anuncios que sean de interés particular a usuarios específicos / describe fin(es)};
 </p>

 <p class="sub">2. La mayoría de navegadores permiten que te niegues a aceptar cookies, por ejemplo:
 </p>
 <p class="sub"> 1. en Internet Explorer (versión 10) puedes bloquear las cookies usando la administración automática de cookies haciendo clic en “Herramientas”, “Opciones de Internet”, “Privacidad” y luego “Avanzado”.
 </p>
 <p class="sub">2. en Firefox (versión 24) puedes bloquear todas las cookies haciendo clic en “Herramientas”, “Opciones”, “Privacidad”, seleccionando “Usar configuración personalizada para el historial” desde el menú desplegable y desmarcando “Aceptar cookies de sitios”; y
 </p>
 <p class="sub"> 3. en Chrome (versión 29) puedes bloquear todas las cookies al acceder al menú de “Privacidad y seguridad” y haciendo clic en “Configuración”, “Avanzada” y luego desmarcas “Permitir que los sitios guarden y lean datos de cookies” bajo el encabezado de Cookies.Bloquear las cookies tendrá un impacto negativo en la usabilidad de muchos sitios web. Si bloqueas las cookies, no podrás usar todas las funciones de tu sitio web.
 </p>
 <p class="sub">3. Puedes eliminar las cookies ya almacenadas en tu ordenador, por ejemplo:
 </p>
 <p class="sub"> 1. en Internet Explorer (versión 10), debes eliminar manualmente los ficheros de cookies (puedes encontrar instrucciones para hacerlo en http://support.microsoft.com/kb/278835 );
 </p>
 <p class="sub">2. en Firefox (versión 24) puedes eliminar todas las cookies haciendo clic en “Herramientas”, “Opciones”, “Privacidad”, seleccionando “Usar configuración personalizada para el historial” y haciendo clic en “Mostrar cookies” y luego haciendo clic en “Eliminar todas las cookies”; y
 </p>
 <p class="sub">3. en Chrome (versión 29) puedes eliminar todas las cookies al acceder al menú de “Privacidad y seguridad” y haciendo clic en “Configuración”, “Avanzada” y luego seleccionando “Eliminar cookies y otros datos de sitios y plugins” antes de hacer clic en “Eliminar datos de navegación”.
 </p>
 <p class="sub">4. Eliminar las cookies tendrá un impacto negativo en la usabilidad de muchos sitios web.
 </p>
 
	</section>
<footer>

<div class="row footer_row">
    <div class="foot" class="copyright">
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;
        <script>
            document.write(new Date().getFullYear());
        </script> Reservk Todos los derechos reservados.
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    </div>
</div>
<input type="image" src="../../assets/images/logo-footer.png" class="imgfoot">
</div>
</footer>



=======
	include 'plantillacondiciones.php';

	$pdf = new PDF();
	$pdf->AliasNbPages();
	$pdf->AddPage();
	
	
	$pdf->Cell(50,5,'',0,1);
	
	$pdf->SetFont('Arial','B',10);
	
	//convertimos el texto a utf8
	$texto = utf8_decode('A. Introducción

1. La privacidad de los visitantes de nuestro sitio web es muy importante y estamos comprometidos a protegerla. Esta política explica qué haremos con su información personal.
Cuando usted nos visita por primera vez y da su consentimiento para el uso que le damos a las cookies, de conformidad con los términos de esta política, usted nos da permiso para usar cookies cada vez que visita nuestro sitio web.

2. Cuando usted nos visita por primera vez y da su consentimiento para el uso que le damos a las cookies, de conformidad con los términos de esta política, usted nos da permiso para usar cookies cada vez que visita nuestro sitio web.


B. Crédito
Este documento fue creado usando una plantilla de SEQ Legal (seqlegal.com) y modificada por Website Planet (www.websiteplanet.com).

C. Recopilar información personal

Los siguientes tipos de información personal pueden ser recopilados, almacenados y usados:

1. información sobre su ordenador, incluyendo su dirección IP, ubicación geográfica, tipo y versión de navegador, y sistema operativo;

2. información sobre sus visitas y uso de este sitio web, incluidas las fuentes de referencia, duración de la visita, visitas de la página y rutas de navegación del sitio web;

3. información que introduzca al crear una cuenta en nuestro sitio web, como la dirección de correo electrónico;

4. información que introduzca al crear un perfil en nuestro sitio web, por ejemplo, su nombre, foto de perfil, género, cumpleaños, estado civil, intereses y pasatiempos, información sobre formación y empleo;

5. información que introduzca para suscribirse a nuestros correos y boletines, como su nombre y dirección de correo electrónico;

6. información que introduzca mientras usa los servicios en nuestro sitio web;

7. información que se genera mientras usa nuestro sitio web, incluido cuándo, qué tan a menudo y bajo qué circunstancias lo use;

8. información sobre cualquier aspecto relacionado con su compra, servicios que use o transacciones que haga a través del sitio web, incluido su nombre, dirección, número telefónico, dirección de correo electrónico e información de tarjeta de crédito;

9. información que publique en nuestro sitio web con la intención de que sea publicada en Internet, lo que incluye su nombre de usuario, fotos de perfil y contenido de sus publicaciones;

10. información contenida en cualquiera de las comunicaciones que nos envía a través de correo electrónico o de nuestro sitio web, incluido el contenido de la comunicación y metadatos;

11. cualquier otra información personal que nos envíe.

Antes de divulgarnos la información personal de otra persona, primero debe obtener el consentimiento de esa persona, tanto para la divulgación como para el procesamiento de esa información personal de acuerdo con esta política;


D. Usar su información personal

La información personal que nos envíe a través de nuestro sitio web será usada con los fines especificados en esta política o en las páginas relevantes del sitio web. Podemos usar su información personal para los siguientes fines:

1. administrar nuestro sitio web y negocio;
2. activar el uso de servicios disponibles en nuestro sitio web;
3. enviarle artículos comprados a través de nuestro sitio web;
4. suministrar servicios comprados a través de nuestro sitio web;
5. enviar extractos, facturas y recordatorios de pago y recopilar datos suyos;
6. enviar comunicaciones comerciales (no de marketing);
7. enviar notificaciones de correo electrónico que ha solicitado específicamente;
8. enviar un boletín de correo electrónico, si usted lo ha solicitado (puede informarnos en cualquier momento si no desea seguir suscrito al boletín);
9. enviar comunicaciones de marketing relacionadas con nuestro negocio, o los negocios de terceros cuidadosamente seleccionados, que consideramos que serán de su interés, por correo postal o donde haya aceptado específicamente a esto, por correo electrónico o tecnologías similares (nos puede informar en cualquier momento si no desea seguir recibiendo las comunicaciones de marketing);
10. dar información estadística a terceros sobre nuestros usuarios (pero esos terceros no podrán identificar a ningún usuario individual con esa información):
11. dar respuesta a las preguntas y quejas suyas o sobre usted, relacionadas con nuestro sitio web;
12. mantener protegido el sitio web y evitar el fraude;
13. verificar el cumplimiento de los términos y condiciones que rigen sobre el uso de nuestro sitio web (incluido la monitorización de mensajes privados a través del servicio de mensajería privada de nuestro sitio web); 
14. y otros usos.

Si usted envía información personal para su publicación en nuestro sitio web, publicaremos y usaremos esa información de conformidad con la licencia que usted nos confiere.
Sus ajustes de privacidad pueden usarse para limitar la publicación de nuestra información en nuestro sitio web y puede ajustarse usando controles de privacidad en el sitio web.
Sin su consentimiento explícito no proporcionaremos su información personal a ningún tercero para su marketing directo o el de otro tercero.





E. Divulgar información personal

Podremos divulgar su información personal a cualquiera de nuestros empleados, oficiales, aseguradores, consejeros profesionales, agentes, proveedores o contratistas, como sea razonablemente necesario para los fines descritos en esta política.

Podremos divulgar su información personal a cualquier miembro de nuestro grupo de empresas (esto incluye subsidiarios, nuestro grupo y todas sus subsidiarias), como sea razonablemente necesario para los fines descritos en esta política.

Podemos divulgar su información personal:

1. hasta lo que sea requerido por la ley;
2. en relación con cualquier procedimiento legal actual o prospectivo;
3. para establecer, ejercer o defender nuestros derechos legales (incluido proporcionar información personal a otros con el fin de evitar fraudes y reducir el riesgo crediticio);
4. al comprador (o comprador prospectivo) de cualquier negocio o activo que estemos vendiendo o estemos contemplando vender; y
5. a cualquier persona que creamos razonablemente que podrá aplicar a una corte o a otra autoridad competente para solicitar la divulgación de esa información personal, y que, bajo nuestra opinión razonable, dicha corte o autoridad tendrá una probabilidad razonable de ordenar la divulgación de dicha información personal.

Con excepción de lo establecido por la ley, no proporcionaremos su información personal a terceros.


E. Divulgar información personal

Podremos divulgar su información personal a cualquiera de nuestros empleados, oficiales, aseguradores, consejeros profesionales, agentes, proveedores o contratistas, como sea razonablemente necesario para los fines descritos en esta política.

Podremos divulgar su información personal a cualquier miembro de nuestro grupo de empresas (esto incluye subsidiarios, nuestro grupo y todas sus subsidiarias), como sea razonablemente necesario para los fines descritos en esta política.

Podemos divulgar su información personal:

1. hasta lo que sea requerido por la ley;
2. en relación con cualquier procedimiento legal actual o prospectivo;
3. para establecer, ejercer o defender nuestros derechos legales (incluido proporcionar información personal a otros con el fin de evitar fraudes y reducir el riesgo crediticio);
4. al comprador (o comprador prospectivo) de cualquier negocio o activo que estemos vendiendo o estemos contemplando vender; y
5. a cualquier persona que creamos razonablemente que podrá aplicar a una corte o a otra autoridad competente para solicitar la divulgación de esa información personal, y que, bajo nuestra opinión razonable, dicha corte o autoridad tendrá una probabilidad razonable de ordenar la divulgación de dicha información personal.

Con excepción de lo establecido por la ley, no proporcionaremos su información personal a terceros.


F. International data transfers

1. La información que recopilamos puede ser almacenada, procesada y transferida entre cualquiera de los países en que operamos, con el fin de permitirnos usar la información de conformidad con esta política.
2. La información que recopilamos puede ser transferida a los siguientes países donde no tenemos leyes de protección de datos equivalentes a las vigentes en el Espacio Económico Europeo: Estados Unidos de América, Rusia, Japón, China e India.
3. La información personal que publique en nuestro sitio web o envíe para su publicación en nuestro sitio web, puede estar disponible a través de Internet, en todo el mundo. No podemos evitar el uso o mal uso de dicha información por parte de otros.
4. Usted acepta explícitamente a las transferencias de información personal descritas en esta sección F.

G. Conservar información personal

1. Esta sección G especifica nuestras políticas y procedimientos de conservación de información personal, diseñadas para ayudar a garantizar que cumplimos con nuestras obligaciones legales con respecto a la conservación y eliminación de información personal.
2. La información personal que procesamos para cualquier fin o fines no debe ser almacenada más tiempo de lo necesario para ese fin o fines.
3. Sin perjuicio del artículo G-2, usualmente eliminaremos los datos personales que estén dentro de las categorías mencionadas a continuación en la fecha y hora determinadas a continuación:
        los datos personales se eliminarán el {INTRODUCE FECHA/HORA}; y
        {INTRODUCE FECHAS/HORAS ADICIONALES}.
4. A pesar de las otras cláusulas de esta sección G, conservaremos los documentos (incluidos documentos electrónicos) que contengan datos personales:
        en medida de lo que sea requerido por la ley;
5. si creemos que los documentos pueden ser relevantes para cualquier procedimiento legal actual o prospectivo; y
        para establecer, ejercer o defender nuestros derechos legales (incluido proporcionar información personal a otros con el fin de evitar fraudes y reducir el riesgo crediticio).


H. Seguridad de su información personal

1. Tomaremos precauciones razonables técnicas y organizacionales para evitar la pérdida, mal uso o alteración de su información personal.
2. Almacenaremos toda la información personal que nos dé en nuestros servidores seguros (protegidos por contraseña y cortafuegos).
3. Todas las transacciones financieras electrónicas realizadas a través de nuestro sitio web estarán protegidas por tecnología de cifrado.
4. Usted acepta que la transmisión de información en Internet es inherentemente insegura y que no podemos garantizar la seguridad de los datos enviados a través de Internet.
5. Usted es responsable de mantener de forma confidencial la contraseña que use para acceder a nuestro sitio web, y nosotros no le pediremos su contraseña (exceptuando para iniciar sesión en nuestro sitio web).
	
I. Enmiendas
	
	Es posible que actualicemos esta política de vez en cuando al publicar una nueva versión en nuestro sitio web. Usted debe comprobar ocasionalmente esta página para asegurarse de que entiende cualquier cambio a esta política. Es posible que le notifiquemos de cambios a esta política a través de correo electrónico o a través del sistema de mensajería privada de nuestro sitio web.
	
J. Sus derechos
	
	Usted puede solicitarnos que le proporcionemos cualquier información personal que tengamos de usted; la entrega de dicha información estará sujeta a lo siguiente:
	
		1. el pago de una tarifa de {INTRODUCE LA TARIFA SI APLICA}; y
		2. que usted presenté evidencia apropiada sobre su identidad ({AJUSTA EL TEXTO PARA REFLEJAR TU POLÍTICA para este fin, usualmente aceptaremos una fotocopia del pasaporte certificada por un notario y una copia original de una factura que muestre su dirección actual}).
	
	Podemos retener información personal que usted nos solicite en la medida de lo permitido por la ley.
	
	Usted puede solicitarnos en cualquier momento que no procesemos su información personal para fines de marketing.
	
	En la práctica, usted usualmente aceptará explícitamente de antemano a que usemos su información personal para fines de marketing, o le daremos la oportunidad de que decida que no se use su información personal para fines de marketing.		


	K. Sitios web de terceros

	Nuestro sitio web incluye hiperenlaces a, e información de, sitios web de terceros. No tenemos control ni somos responsables por las políticas de privacidad y prácticas de terceros.
	
	L. Actualizar información
	
	Por favor, infórmenos si la información personal que tenemos de usted necesita ser corregida o actualizada.


	M. Cookies

Nuestro sitio web usa cookies. Una cookie es un fichero que contiene un identificador (una cadena de letras y números) que se envía desde un servidor web a un navegador web y se almacena en el navegador. El identificador es enviado de vuelta al servidor cada vez que el navegador solicita una página del servidor. Las cookies pueden ser “persistentes” o de “sesión”. Una cookie persistente será almacenada por una navegador web y permanecerá válida hasta su fecha de caducidad, a menos que el usuario la elimine antes de la fecha de caducidad. Una cookie de sesión caducará al final de la sesión del usuario, cuando se cierre el navegador. Las cookies típicamente no contienen ninguna información que identifique personalmente al usuario sino información personal que almacenamos sobre usted que puede relacionarse con la información almacenada y obtenida de las cookies. {SELECCIONA LA FORMA DE DECIRLO MÁS PRECISA Solo usamos cookies de sesión / Solo usamos cookies persistentes / Solo usamos cookies de sesión y persistentes en nuestro sitio web.}

    1. Los nombres de las cookies que podemos usar en nuestro sitio web y los fines para las que son usadas, están especificadas a continuación:
        1. usamos Google Analytics y Adwords en nuestro sitio web para reconocer un ordenador cuando un usuario {INCLUYE TODO PARA LO QUE SON USADAS LAS COOKIES EN TU SITIO visita el sitio web / monitorizar usuarios mientras navegan por el sitio web / permitir el uso de un carrito de compras en el sitio web / mejorar la usabilidad del sitio / analizar el uso del sitio web / administrar el sitio web / evitar fraude y mejorar la seguridad del sitio web / personalizar el sitio web para cada usuario / personalizar los anuncios que sean de interés particular a usuarios específicos / describe fin(es)};
    2. La mayoría de navegadores permiten que te niegues a aceptar cookies, por ejemplo:
        1. en Internet Explorer (versión 10) puedes bloquear las cookies usando la administración automática de cookies haciendo clic en “Herramientas”, “Opciones de Internet”, “Privacidad” y luego “Avanzado”.
        2. en Firefox (versión 24) puedes bloquear todas las cookies haciendo clic en “Herramientas”, “Opciones”, “Privacidad”, seleccionando “Usar configuración personalizada para el historial” desde el menú desplegable y desmarcando “Aceptar cookies de sitios”; y
        3. en Chrome (versión 29) puedes bloquear todas las cookies al acceder al menú de “Privacidad y seguridad” y haciendo clic en “Configuración”, “Avanzada” y luego desmarcas “Permitir que los sitios guarden y lean datos de cookies” bajo el encabezado de Cookies.Bloquear las cookies tendrá un impacto negativo en la usabilidad de muchos sitios web. Si bloqueas las cookies, no podrás usar todas las funciones de tu sitio web.
    3. Puedes eliminar las cookies ya almacenadas en tu ordenador, por ejemplo:
        1. en Internet Explorer (versión 10), debes eliminar manualmente los ficheros de cookies (puedes encontrar instrucciones para hacerlo en http://support.microsoft.com/kb/278835 );
        2. en Firefox (versión 24) puedes eliminar todas las cookies haciendo clic en “Herramientas”, “Opciones”, “Privacidad”, seleccionando “Usar configuración personalizada para el historial” y haciendo clic en “Mostrar cookies” y luego haciendo clic en “Eliminar todas las cookies”; y
        3. en Chrome (versión 29) puedes eliminar todas las cookies al acceder al menú de “Privacidad y seguridad” y haciendo clic en “Configuración”, “Avanzada” y luego seleccionando “Eliminar cookies y otros datos de sitios y plugins” antes de hacer clic en “Eliminar datos de navegación”.
    4. Eliminar las cookies tendrá un impacto negativo en la usabilidad de muchos sitios web.');

$pdf->Cell(50,5,'',0,1);


	$pdf->MultiCell(190,5,$texto);

	
	$pdf->SetFont('Arial','',10);

	$pdf->Output();
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
