<?php

use frontend\models\Ambiente;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Tipo_de_dano',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Detalle_del_dano',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'Reserva_id_reserva',
    ],
  
    [
        'label'=>'Ambiente',
        'value' => function ($data) {
            return $data->reserva->ambiente->title;
        },
        'filter' => ArrayHelper::map(Ambiente::find()->all(), 'id_ambiente', 'title'),
        'class' => '\kartik\grid\DataColumn',
        'vAlign' => GridView::ALIGN_MIDDLE,
        'filterType' => GridView::FILTER_SELECT2,   
        'filterWidgetOptions' => [
            'options' => ['placeholder' => 'Seleccionar...'],
            'pluginOptions' => [
                'allowClear' => TRUE
            ],
        ],
    ],
  
    [
        'class' => 'kartik\grid\ActionColumn',
        'header'=>'Ver',
        'template'=>'{view}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Ver','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Actualizar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Eliminar', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'¿Está seguro?',
                          'data-confirm-message'=>'¿Está seguro que desea eliminar este registro de daño?'], 
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header'=>'Acciones',
        'visible'=>Yii::$app->user->can('r-admin'),
        'template'=>'{update}{delete}',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'updateOptions'=>['role'=>'modal-remote','title'=>'Actualizar', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Eliminar', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'¿Está seguro?',
                          'data-confirm-message'=>'¿Está seguro que desea eliminar este registro de daño?'], 
    ],

];   