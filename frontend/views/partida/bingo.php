<?php

use kartik\form\ActiveForm;
use kartik\helpers\Html;

$this->title = 'Bingo | Inicio';


$var = [ 1000 => 'Normal', 2000 => 'Flash',  4000 => 'Rápido'];
$pesos = [ 1000 => '1000 Pesos', 2000 => '2000 Pesos',  3000 => '3000 Pesos',4000 => '4000 Pesos',5000 => '5000 Pesos'];

?>

<div id="espacio"></div>
<div id="izquierdo">

    <div id="titulo">
        <img src="img/icon.png" height="100" width="100">
        <h1> Bingo</h1>

    </div>
    <div class="partida-form">

        <?php
        $form = ActiveForm::begin(); ?>
        <div id="datos">
            
            <?= $form->field($model, 'velocidad')->dropDownList($var, ['prompt' => 'Seleccione Uno' ,'id'=>'velo']); ?>
            <?= $form->field($model, 'numero_jugadores')->textInput(['id'=>'njugadores','type'=>'number']) ?>
            <?= $form->field($model, 'valor_carton')->dropDownList($pesos, ['prompt' => 'Seleccione Uno' ,'id'=>'valorc']) ?>
            <hr>
            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'COMENZAR' : 'ACTUALIZAR', ['class' => $model->isNewRecord ? 'btn btn-default' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>
            <hr>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>

<?= $this->render('partidasDisponibles', [
    'searchModel' => $searchModel,
    'dataProvider' => $dataProvider,
    ]) ?>

