<?php

use kartik\form\ActiveForm;
use kartik\helpers\Html;

$this->title = 'Bingo | Jugando';


?>

<div class="no-playing">
    <input type="hidden" value="<?= $play->id_partida; ?>" id="game">
    <div class="loading-players">
        <div class="title-loading">
            <h1><strong>Iniciando Partida...</strong></h1>
        </div>
        <div class="subtitle-loading">
            <h2>Faltan <strong><span id="number_players">0</span></strong> jugadores.</h2>
        </div>
    </div>
    <div id="espacio"></div>
    <div id="izquierdo">
        <div id="titulo">
        </div>
        <div id="datos">
            <input type="hidden" id="njugadores" value="<?= $play->numero_jugadores; ?>">
            <input type="hidden" id="velo" value="<?= $play->velocidad; ?>">
            <input type="hidden" id="valorc" value="<?= $play->valor_carton; ?>">
        </div>
    </div>
    <div id="derecho"><br><br><br>
        <div id="info">
            <div id="espacio"></div><br>

        </div>
    </div>
</div>

<?= $this->registerJsFile(Yii::$app->request->baseUrl . '/..' . '/assets/js/funciones.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ?>