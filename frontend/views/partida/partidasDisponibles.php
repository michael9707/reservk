<?php

use frontend\models\Velocidad;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PartidaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="partida-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
          //  'pjax'=>true,
            'columns' =>[
                [
                    'class' => 'kartik\grid\CheckboxColumn',
                    'width' => '20px',
                ],
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                    [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'id_partida',
                ],
                [
                    'label'=>'Velocidad',
                    'attribute'=>'velocidad',
                    'value' => function ($data) {
                        return $data->velocidad0->nombre_velocidad;
                    },
                    'filter' => ArrayHelper::map(Velocidad::find()->all(), 'id_velocidad', 'nombre_velocidad'),
                    'class' => '\kartik\grid\DataColumn',
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'options' => ['placeholder' => 'Seleccionar...'],
                        'pluginOptions' => [
                            'allowClear' => TRUE
                        ],
                    ],
                ],
          
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'numero_jugadores',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'valor_carton',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'ganador',
                    'value'=>function($data){
                        if(empty($data->ganador)){
                            return 'No disponible';
                        }else{
                            return $data->ganador;
                        }
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'creado_por',
                ],
                // [
                    // 'class'=>'\kartik\grid\DataColumn',
                    // 'attribute'=>'creado_el',
                // ],
                // [
                    // 'class'=>'\kartik\grid\DataColumn',
                    // 'attribute'=>'actualizado_por',
                // ],
                // [
                    // 'class'=>'\kartik\grid\DataColumn',
                    // 'attribute'=>'actualizado_el',
                // ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'template' => '{play}',
                    'buttons' => [
                        'play' => function ($url, $model) {
                            return Html::a('<span class="fa fa-hand-paper-o">  </span>  Ingresar', $url, [
                                        'title' => Yii::t('app', 'Info'),
                                        'class'=>'btn btn-success'
                            ]);
                
                        }
                
                    ],
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) { 
                            return Url::to([$action,'id'=>$key]);
                    },
                
                ],
            
            ],
            'toolbar'=> [
               
            ],          
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Partidas ',
                'before'=>'<em>* Seleccione una partida para iniciar un juego disponible</em>',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
