<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Partida */
?>
<div class="partida-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
