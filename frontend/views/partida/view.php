<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Partida */
?>
<div class="partida-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_partida',
            'velocidad',
            'numero_jugadores',
            'valor_carton',
            'creado_por',
            'creado_el',
            'actualizado_por',
            'actualizado_el',
        ],
    ]) ?>

</div>
