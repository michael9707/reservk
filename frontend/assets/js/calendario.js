$(document).ready(function(e) {



    $('.alert').css("display", "inherit").hide();

    //Creo el evento
    $("#create_event").submit(function(e) {
        e.preventDefault();
        var thistarget = this.target;
        jQuery.ajax({
            data: $(this).serialize(),
            dataType: 'json',
            url: this.action,
            type: this.method,
            error: function(r) {
                $('.alert-danger').html("Ocurrió un error inesperado");
            },
            success: function(results) {
                $('.alert').fadeOut();
                if (results.status == "error") {
                    $('.alert-danger').html(results.message).fadeIn(300)
                } else {
                    $('.alert-success').html(results.message).fadeIn(300);
                    setTimeout(function() {
                        $('#add_evento').modal('hide');
                    }, 2000);


                }

            }
        })
    });


    //Redireciono al controlador de eventos 


    var date = new Date();
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth() + 1).toString().length == 1 ? "0" + (date.getMonth() + 1).toString() : (date.getMonth() + 1).toString();
    var dd = (date.getDate()).toString().length == 1 ? "0" + (date.getDate()).toString() : (date.getDate()).toString();


    var options = {


        modal: '#events-modal',


        modal_type: 'iframe',


        events_source: url_ + '/web/reserva/events',


        view: 'month',


        day: yyyy + "-" + mm + "-" + dd,

        language: 'es-ES',

        tmpl_path: url_ + '/assets/tmpls/',
        tmpl_cache: false,

        time_start: '08:00',


        time_end: '22:00',

        time_split: '30',


        width: '100%',

        onAfterEventsLoad: function(events) {
            if (!events) {
                return;
            }
            var list = $('#eventlist');
            list.html('');

            $.each(events, function(key, val) {
                $(document.createElement('li'))
                    .html('<a href="' + val.url + '">' + val.title + '</a>')
                    .appendTo(list);
            });
        },
        onAfterViewLoad: function(view) {
            $('.page-header h2').text(this.getTitle());
            $('.btn-group button').removeClass('active');
            $('button[data-calendar-view="' + view + '"]').addClass('active');
        },
        classes: {
            months: {
                general: 'label'
            }
        }
    };

    // id del div donde se mostrara el calendario
    var calendar = $('#calendar').calendar(options);

    $('.btn-group button[data-calendar-nav]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.navigate($this.data('calendar-nav'));
        });
    });

    $('.btn-group button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.view($this.data('calendar-view'));
        });
    });

    $('#first_day').change(function() {
        var value = $(this).val();
        value = value.length ? parseInt(value) : null;
        calendar.setOptions({
            first_day: value
        });
        calendar.view();
    });



    let calling_ajax = function() {
        $this = $("#event")
        var thistarget = $this.target;

        $.ajax({
            data: $("#event").serialize(),
            dataType: 'json',
            url: $this.attr("action"),
            type: $this.attr("method"),
            error: function(r) {
                $('.alert-danger').html("Ocurrió un error inesperado");
            },
            success: function(results) {
                $('.alert').fadeOut();
                return (results.status == "error") ? $('.alert-danger').html(results.message).fadeIn(300) : $('.alert-success').html(results.message).fadeIn(300);
            }
        })
    }

    $("#modificar").click(function(e) {
        e.preventDefault();
        $(".type_e").val("modificar");
        calling_ajax();
        /* if (!$(this).hasClass("actualizar")) {
            $(this).addClass("actualizar").html("Actualizar");
            $('.dates').attr('readonly', false);
        } else {
            $(".type_e").val("modificar");
            calling_ajax();
        } */
    });
    $("#eliminar").click(function(e) {
        e.preventDefault();
        $(".type_e").val("eliminar");
        calling_ajax();
    });


    $('#from').datetimepicker({
        language: 'es',
        minDate: new Date(),
        defaultDate: new Date(),
    });
    $('#to').datetimepicker({
        language: 'es',
        minDate: new Date(),
        defaultDate: new Date(),
    });

    let sweat = function() {
        Swal.mixin({

            confirmButtonText: 'Siguiente ',
            showCancelButton: false,

            progressSteps: ['1', '2', '3', '4', '5', '6', '7'],

        }).queue([{
                title: 'Condiciones de préstamo',
                text: 'Utilizar correctamente los implementos  que se encuentran dentro de los espacios'
            },
            {
                title: '',
                text: 'No dejar sucio el espacio'
            },
            {
                title: '',
                text: 'No mover de puesto los implementos del espacio'
            },
            {
                title: '',
                text: 'Tener control del grupo que ingrese'
            },
            {
                title: '',
                text: 'Dejar en buen estado los implementos del espacio'
            },
            {
                title: '',
                text: 'Notificar el estado del ambiente cuando entre y cuando sale'
            },
            {

                title: '¡IMPORTANTE!',
                icon: 'warning',
                text: 'Se generalizara un bloqueo de la cuenta si tiene más de tres registros de daño en el mismo ambiente en el mismo mes, para activarla de nuevo decirle al admin   '
            }

        ]).then((result) => {
            if (result.value) {
                const answers = JSON.stringify(result.value)
                Swal.fire({
                    title: '',
                    html: `
<<<<<<< HEAD
                    Está de acuerdo con las condiciones de préstamo :`,
=======
            Esta deacuerdo con las condiciones de prestamo:`,
>>>>>>> 6ce1ff512c46b4d30419d087853cb81a87e808c7
                    confirmButtonText: '<a style="color: white;">Acepto</a>',
                    confirmButtonColor: '#F78536',
                })
            }
        })
    }


    if ($('#count').val() == "1") {
        sweat();
    }


});