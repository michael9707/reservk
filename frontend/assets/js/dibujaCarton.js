/**
 * Dibuja un cartón de un array pasado como parámetro
 * @param {Array} carton Contiene un array con los numeros de las filas y columnas y huecos
 */
function dibujaCarton(carton) {
	//Generamos la tabla
	var tabla = document.createElement("table");
	tabla.setAttribute("id", "carton");
	tabla.setAttribute("border", "3");
	tabla.classList.add('carton');
	//Genero las filas
	for (var i = 0; i < carton.length; i++) {
		var fila = document.createElement("tr");
		//Genero las columnas
		for (var j = 0; j < carton[i].length; j++) {
			var celda = document.createElement("td");
			celda.setAttribute("id", i + "/" + j);
			//Si la celda es -1 es que está hueca
			if (carton[i][j].valor === -1) {
				celda.classList.add('hueco'); //Le asociamos la clase .hueco que tiene la imagen del bombo
			}
			else {
				celda.innerHTML = carton[i][j].valor;
				celda.addEventListener("click", function () { marcar(this.id, carton); }, false); //Establecemos listener de tipo click 	
			}
			fila.appendChild(celda); //Añadimos la celda a la fila		
		}
		tabla.appendChild(fila); //Añadimos la fila al carton
	}
	//Agregamos la carton al div correspondiente
	var sitio = document.getElementById("derecho");
	sitio.appendChild(tabla);
	//Dibujo los botones del bingo
	$("#derecho").append("<br><br><div id='bbingo'><button class='btn btn-default btn-lg'><b>¡Bingo!</b></button></div>");
	$("#bbingo button").click(cantaBingo);
	$("#datos").append("<button id='reset' class='btn btn-default'><b>REINICIAR</b></button>");
	$("#reset").click(resetear);
}
